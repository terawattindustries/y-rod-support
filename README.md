Y-Rod Supports
--------------
Y-Rod supports for Prism Mendel.  Could be used with Mendelmax or other similarly-designed repraps.  Designed to attach to top of extrusion so Y-Rod can rest above.


Part Assembly Instructions
--------------------------
* Attach support to extrusion using t-slot nuts
* Insert M3 captive nut into captive nut slot
* Insert M3x16mm screw into screw hole
* Insert y-rod support into support
* Tighten M3 screw onto support lightly


Building STL from SCAD Source
-----------------------------
An ANT script is provided for automated openscad compilation of source files in this project.  You must have ANT installed to use this build script.  ANT is based on Java and runs on Windows and Linux platforms.  The ANT build script provided with this project is a work-in-progress but it's actively used and maintained.

Configuring ANT
---------------
The build.xml in the project depends on the Flaka ANT add-on.  To install Flaka in your environment, enter the following from a command line interface (unix):
cp [path-to-this-project]/tools/build/ant-flaka-1.02.02.jar [path-to-ANT-installation]/lib

In other words, Flaka installs like any other ANT add-on.  If you have trouble installing the library, or don't know where ANT is installed, try typing "ant -diagnostics" from a command line.

Targets:
To list the targets provided by this build.xml enter "ant -p" from a command line interface.

Building this Part Using ANT
--------------------------------
To build the SCAD source files for the first time you must edit unix.settings.properties (or windows.settings.properties).  Set the path to the executable for openscad, for example (on OS X):
exec.path.oscad=/Applications/OpenSCAD.app/Contents/MacOS/OpenSCAD

Once you've edited unix.settings.properties run the following from a Unix or Windows command line to compile the SCAD source files:
ant

